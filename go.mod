module gitlab.com/zendrulat123/goflutters

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.12
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/rs/cors v1.7.0
	gitlab.com/zendrulat123/gow v0.0.0-20200321231026-261267f3805f
)
